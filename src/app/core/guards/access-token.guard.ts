import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ApiService } from '../services/api/api.service';

@Injectable({
  providedIn: 'root'
})
export class AccessTokenGuard implements CanActivate {
  constructor(
    private router: Router,
    private apiService: ApiService,
  ) { }

  canActivate(): any {
    const token = this.apiService.getToken();
    if (token) {
      return true;
    }

    this.router.navigate(['auth/login']).then(() => {
      return false;
    });
  }
}
