import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  private url = environment.url;

  login(credentials: object) {
    return this.http.post(this.url + 'login', credentials).toPromise();
  }
}
