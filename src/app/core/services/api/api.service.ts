import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  constructor(
    private http: HttpClient
  ){}

  
  private url = environment.url;

  getToken() {
    let token: any = localStorage.getItem('token')
    if (token) {
      token = JSON.parse(atob(token))
    }
    return token && token.token ? token.token : false;
  }

  // Obtain data //
  getDataObjects(model: string, filter?: any) {
      return this.http.get(this.url + model + (filter ? `?${filter.param}=${filter.value}` : ''), { headers: new HttpHeaders({ Authorization: this.getToken() }) }).toPromise();
  }

  // Obtain object per id //
  getDataObject(model: string, id: string) {
      return this.http.get(this.url + model + '/' + id, { headers: new HttpHeaders({ Authorization: this.getToken() }) }).toPromise();
  }

  // Obtain object per id //
  editDataObject(model: string, id: string, data: any) {
      return this.http.patch(this.url + model + '/' + id, data,{ headers: new HttpHeaders({ Authorization: this.getToken() }) }).toPromise();
  }
}
