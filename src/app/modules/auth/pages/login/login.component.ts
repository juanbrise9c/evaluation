import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../../core/services/auth/auth.service';

@Component({
  selector: 'a6s-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(
    private authServie: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  loginForm = this.fb.group({
    email: new FormControl({value: '', disabled: false}, Validators.required),
    password: new FormControl({value: '', disabled: false}, Validators.required)
  });

  perfom() {
    const { email, password } = this.loginForm.value;
    const propiertie = email.includes('@') ? 'email' : 'username';
    const body = {
      [propiertie]: email,
      password: password
    };

    this.authServie.login(body)
      .then((data: any) => {
        if(data) {
          localStorage.setItem('token', btoa(JSON.stringify(data)));
          this.router.navigate(['users']);
        }
      })
      .catch((err: any) => {
        console.log(err);
      })
  }
}

