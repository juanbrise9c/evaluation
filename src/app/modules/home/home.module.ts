import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { UsersComponent } from './pages/users/users.component';
import { MaterialModule } from '../../shared/material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from '../../core/services/auth/auth.service';
import { ApiService } from '../../core/services/api/api.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    UsersComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthService,
    ApiService
  ]
})
export class HomeModule { }
