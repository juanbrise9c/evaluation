import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material/sidenav';
import { ApiService } from '../../../../core/services/api/api.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'a6s-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  constructor(
    private observer: BreakpointObserver,
    private ApiService: ApiService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;
  dataSource: any;
  userSelected: any;
  editUser: boolean = false;
  displayedColumns: string[] = ['id' ,'email', 'first_name', 'last_name'];

  userForm = this.fb.group({
    first_name: new FormControl({value: '', disabled: false}, Validators.required),
    last_name: new FormControl({value: '', disabled: false}, Validators.required),
    email: new FormControl({value: '', disabled: false}, Validators.required)
  });

  ngOnInit() {
    this.getUsers();
  }

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 800px)']).subscribe((res) => {
      if (res.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.close();
      }
    });

    this.dataSource.paginator = this.paginator;
  }

  getUsers() {
    this.ApiService.getDataObjects('users')
      .then((data: any) => {
        this.dataSource = new MatTableDataSource(data.data);
      })
      .catch((err) => {
        console.log(err);
      })
  }

  getPosts(user) {
    this.ApiService.getDataObjects('posts', { param: 'userId', value: user.id} )
      .then((data: any) => {
        user.posts = data.data;
        this.userSelected = user;
        this.sidenav.open();
      })
      .catch((err) => {
        console.log(err);
      })
  }

  perfom(){
    const body = {
      ...this.userForm.value,
      avatar: this.userSelected.avatar,
      id: this.userSelected.id
    };

    this.ApiService.editDataObject('users', this.userSelected.id, body)
    .then((data: any) => {
      this.getUsers();
      this.getPosts(data);
      this.editUser = false;
    })
  }

  edit(){
    this.userForm.patchValue(this.userSelected)
    this.editUser = true;
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['auth/login']);
  }
}
