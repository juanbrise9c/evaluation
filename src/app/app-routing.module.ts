import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessTokenGuard } from './core/guards/access-token.guard';

const routes: Routes = [
  { 
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then( m => m.AuthModule )
  },
  { 
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then( m => m.HomeModule ),
    canActivate: [AccessTokenGuard]
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
